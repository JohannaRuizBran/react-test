import React from "react";
import Spinner from "react-bootstrap/Spinner";
import "./Loader.scss";

/**
 * @function Loader: display a loader component to indicate to the user that the data is being loaded.
 * @output component with loader.
 */
function Loader() {
  return (
    <div className="loader">
      <Spinner animation="border" role="status">
        <span className="sr-only">Loading...</span>
      </Spinner>
      <h5>Loading.. </h5>
    </div>
  );
}

export default Loader;
