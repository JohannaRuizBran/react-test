import React from "react";
import "./PostComments.scss";

/**
   * @function PostComments: display all comments for a specific post.
   * @params props: object with post comment list: commentsWithNewElements.
   * @output component with all the comments about the post.
   */
function PostComments(props) {
  return (
    <div className="postcomments-message">
      {props.commentsWithNewElements.map((comment, index) => (
        <div key={index}>
          <div className="postcomments-information">
            <p className="postcomments-title">{`${comment.name}: (${comment.email})`}</p>
            <p>{comment.body}</p>
          </div>
        </div>
      ))}
    </div>
  );
}

export default PostComments;
