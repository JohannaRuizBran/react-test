import React from "react";
import Card from "react-bootstrap/Card";
import "./PostCard.scss";

function PostCard(props) {
  const { id, title, description, showComment } = props;

  /**
   *  @function openPostComments: create the object with the information of the post that was clicked and calls
   * the showComment function to show the comments of the selected post.
   */
  const openPostComments = () => {
    const value = {
      postId: id,
      postCommentVisibility: true,
      postTitle: title,
    };
    showComment(value);
  };

  return (
    <div className="Card">
      <Card style={{ width: "18rem" }}>
        <Card.Body>
          <Card.Title>{title}</Card.Title>
          <Card.Text>{description}</Card.Text>
          <Card.Link id="seeComments" href="#" onClick={openPostComments}>
            See comments
          </Card.Link>
        </Card.Body>
      </Card>
    </div>
  );
}

export default PostCard;
