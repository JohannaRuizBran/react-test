import React, { Component } from "react";
import { updateCommentsList } from "../../Redux/actions";
import { connect } from "react-redux";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";

export class SendMessageForm extends Component {
  state = {
    validated: false,
  };

  /**
   * @function sendMessage: Create the objects to insert the new user comment and call the UpdateCommentList
   *  function to insert that comment in the "comments list" of the selected post.
   * @params Object: has attributes (1) name: user name (2) email: user email, (3) comment: user comment.
   */
  sendMessage = ({ name, email, comment }) => {
    const newComment = {
      body: comment.value,
      email: email.value,
      name: name.value,
      postId: this.props.postId,
    };
    this.props.updateCommentsList(newComment);
  };

  /**
   * @function handleSubmit: check the user input validations and if there is no error 
   * call the sendMessage function and reset the form.
   * @params form event properties object.
   */
  handleSubmit = (event) => {
    const form = event.currentTarget;
    event.preventDefault();
    event.stopPropagation();
    if (form.checkValidity() === false) {
      this.setState({ validated: true });
      return;
    }
    this.sendMessage(form.elements);
    this.setState({ validated: false });
    form.reset();
  };

  render() {
    return (
      <Form
        noValidate
        validated={this.state.validated}
        onSubmit={this.handleSubmit}
      >
        <Form.Row>
          <Form.Group as={Col} md="5" controlId="name">
            <Form.Label>Name</Form.Label>
            <Form.Control size="sm" required type="text" placeholder="Name" />
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            <Form.Control.Feedback type="invalid">
              Please insert the name.
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group as={Col} md="5" controlId="email">
            <Form.Label>Email</Form.Label>
            <Form.Control size="sm" required type="email" placeholder="Email" />
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            <Form.Control.Feedback type="invalid">
              Please insert the Email.
            </Form.Control.Feedback>
          </Form.Group>
        </Form.Row>

        <Form.Row>
          <Form.Group as={Col} md="10" controlId="comment">
            <Form.Label>Comment</Form.Label>
            <Form.Control
              size="sm"
              required
              as="textarea"
              rows="2"
              placeholder="Comment"
            />
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            <Form.Control.Feedback type="invalid">
              Please insert the Comment.
            </Form.Control.Feedback>
          </Form.Group>
        </Form.Row>
        <Form.Row>
          <Button type="submit">Send Comment</Button>
        </Form.Row>
      </Form>
    );
  }
}

const mapDispatchToProps = {
  updateCommentsList,
};

export default connect(null, mapDispatchToProps)(SendMessageForm);
