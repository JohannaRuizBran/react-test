import React from "react";
import { Provider } from "react-redux";
import Posts from "./Containers/Post/Post";
import store from "./Redux/store";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <Provider store={store}>
      <Posts />
    </Provider>
  );
}

export default App;
