import constants from "../Constants/constants";

export const updateCommentsList = (newState) => {
  return {
    type: constants.UPDATE_COMMENTS_LIST,
    newState,
  };
};
