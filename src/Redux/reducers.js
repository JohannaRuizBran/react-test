import constants from "../Constants/constants";
const defaultState = {
  comments: [],
};

const reducer = (state = defaultState, { type, newState }) => {
  switch (type) {
    case constants.UPDATE_COMMENTS_LIST:
      return {
        ...state,
        comments: [...state.comments, newState],
      };
    default:
      return state;
  }
};

export default reducer;
