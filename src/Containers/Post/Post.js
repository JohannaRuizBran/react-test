import React, { Component } from "react";
import PostCard from "../../Components/PostCard/PostCard";
import Loader from "../../Components/Loader/Loader";
import CommentsModal from "../CommentsModal/CommentsModal";

class Post extends Component {
  state = {
    post: [],
    arePostCommentsVisible: false,
    idSelectedPost: 0,
    postTitle: "",
    loading: true,
  };

  async componentDidMount() {
    await fetch("https://jsonplaceholder.typicode.com/posts")
      .then((result) => {
        console.log("dsad", result);
        return result.json();
      })
      .then((post) => {
        console.log("dsad", post);
        this.setState({ ...this.state, post, loading: false });
      })
      .catch((error) => console.log("error: ", error));
  }

  /**
   * @function showComment: sets the state to open and send the information of the selected post.
   * @params Object: has attributes (1) postId: id of the selected post, (2) postCommentVisibility: the new state of the
   * postComments view (boolean), (3)postTitle: title of the selected post.
   */
  showComment({ postId, postCommentVisibility, postTitle }) {
    this.setState({
      ...this.state,
      arePostCommentsVisible: postCommentVisibility,
      idSelectedPost: postId,
      postTitle,
    });
  }

  render() {
    return (
      <div className="App">
        {this.state.loading ? (
          <Loader />
        ) : (
          this.state.post.map((post) => (
            <PostCard
              key={post.id}
              id={post.id}
              title={post.title}
              description={post.body}
              showComment={(value) => this.showComment(value)}
            />
          ))
        )}
        {this.state.idSelectedPost ? (
          <CommentsModal
            isCommentVisible={this.state.arePostCommentsVisible}
            showComment={(value) => this.showComment(value)}
            postId={this.state.idSelectedPost}
            title={this.state.postTitle}
          />
        ) : null}
      </div>
    );
  }
}

export default Post;
