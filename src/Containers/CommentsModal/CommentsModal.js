import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import "./CommentsModal.scss";
import SendMessageForm from "../../Components/SendMessageForm/SendMessageForm";
import PostComments from "../../Components/PostComments/PostComments";
import Loader from "../../Components/Loader/Loader";
import { connect } from "react-redux";

export class CommentsModal extends Component {
  state = {
    comments: [],
    loading: true,
  };

  async componentDidMount() {
    const { postId } = this.props;
    await fetch(
      `https://jsonplaceholder.typicode.com/comments?postId=${postId}`
    )
    .then((result) => result.json())
    .then((comments) => this.setState({ comments, loading: false }))
    .catch((error) => console.log("error: ", error));
  }

  /**
   * @function getPostCommentsInLocalStore: Get the list of comments for a specific post.
   * @params postId: id of the selected post.
   * @outputs return an array with the comments of an specific post.
   */
  getPostCommentsInLocalStore = (postId) => {
    const { comments } = this.props.comments;
    return comments.filter((ele) => ele.postId === postId);
  };

  render() {
    const { isCommentVisible, showComment, postId, title } = this.props;
    const commentsWithNewElements = [
      ...this.state.comments,
      ...this.getPostCommentsInLocalStore(postId),
    ];
    return (
      <>
        <Modal
          show={isCommentVisible}
          onHide={() => showComment(postId, false)}
          size="lg"
          className="comments-content"
          dialogClassName="comments-content"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              {title}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.loading ? (
              <Loader />
            ) : (
              <PostComments commentsWithNewElements={commentsWithNewElements} />
            )}
          </Modal.Body>
          <Modal.Footer>
            <div className="comments-send">
              <SendMessageForm postId={postId} />
            </div>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    comments: state.reducer,
  };
};

export default connect(mapStateToProps)(CommentsModal);
