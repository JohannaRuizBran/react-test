import React from "react";
import Enzyme, { mount, render, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { CommentsModal } from "../Containers/CommentsModal/CommentsModal";
import { Provider } from "react-redux";
import store from "../Redux/store";
Enzyme.configure({ adapter: new Adapter() });

const props = {
  postId: 1,
  comments: { comments: [] },
  isCommentVisible: true,
  showComment: false,
  title: "test",
};

const mockFetchPromise = Promise.reject({
  error: "error"
});
jest.spyOn(global, "fetch").mockImplementation(() => mockFetchPromise);

test("Loader should be displayed if promise is rejected(comments result is not an array)", () => {
  const wrapper = mount(
    <Provider store={store}>
      <CommentsModal {...props} />
    </Provider>
  );
  expect(wrapper.find('Loader').length === 1).toEqual(true);
});
