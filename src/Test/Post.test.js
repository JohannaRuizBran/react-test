import React from "react";
import Enzyme, { mount, render, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import Post from "../Containers/Post/Post";
Enzyme.configure({ adapter: new Adapter() });

const mockFetchPromise = Promise.reject({
  error: "error",
});

test("Loader should be displayed if promise is rejected(comments result is not an array)", () => {
  jest.spyOn(global, "fetch").mockImplementation(() => mockFetchPromise);
  const wrapper = mount(<Post />);
  expect(wrapper.find("Loader").length === 1).toEqual(true);
});