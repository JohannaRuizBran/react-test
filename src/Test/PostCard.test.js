import React from "react";
import Enzyme, { mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import PostCard from "../Components/PostCard/PostCard";

Enzyme.configure({ adapter: new Adapter() });

test("showComment function should be call when See comments link in cliked", () => {
  const showComment = jest.fn();
  const wrapper = mount(
    <PostCard
      id={1}
      title={"test"}
      description={"desc"}
      showComment={showComment}
    />
  );
  wrapper.find('.card-link').at(0).simulate('click');
  expect(showComment).toHaveBeenCalledTimes(1);
});
