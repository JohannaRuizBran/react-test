import React from "react";
import Enzyme, { mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { SendMessageForm } from "../Components/SendMessageForm/SendMessageForm";
Enzyme.configure({ adapter: new Adapter() });

const updateCommentsList = jest.fn();

const props = {
  postId: 1,
  updateCommentsList,
};

test("validate form should not be active if come input data is correct and form is submited", () => {
  const form = mount(<SendMessageForm {...props} />);
  const inputName = form.find("#name");
  inputName.value = "Blah blah";
  const inputEmail = form.find("#email");
  inputEmail.value = "Blah@gmail.com";
  const inputComment = form.find("#comment");
  inputComment.value = "Blah blah";
  form.find("button").at(0).simulate("click");
  expect(form.state("validated")).toBe(false);
});
