## About the project

ReactJS project that fetches from the posts and comments API’s, presents a list of
posts and when any of the items is clicked show the list of comments that are associated with
that specific post. Also allows the user to create a new comment.

## Project structure

|<br/>
|---> Components<br/> 
| |---> Loader <br/>
| |---> PostCard <br/>
| |---> PostComments<br/>
| |---> SendMessageForm <br/>
|<br/>
|---> Constants<br/>
|<br/>
|---> Containers<br/>
| |---> CommentsModal<br/>
| |---> Post<br/>
|<br/>
|---> Redux<br/>
|<br/>
|---> Test<br/>


### `Components` 
All components are located inside this folder.
### `Loader`
Inside this folder is the component to indicate to the user that the data is being loaded.
### `PostCard`
Inside this folder is the component to display the post structure in card style with the title and description.
### `PostComments` 
Inside this folder is the component to display all comments for a specific post.
### `SendMessageForm`
Inside this folder is the component that allows the user to insert new comments through a form.
### `Constants`
Files to be used to insert constant data of the project.
### `Containers`
All class components that are containers.
### `CommentsModal`
Inside this folder is the component to display popup, modal or window that shows all the information of the comments of a selected post.
### `Post`
Inside this folder is the component to display all posts and the information about it.
### `Redux`
Within this folder are all the logics and configurations to implement Redux: actions, reducers and storage.
### `Test`
All system tests files.


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
